import React, { useState } from 'react'
import './style.css'

const initialLap = [
  {
    min: '00',
    sec: '00',
    milli: '00'
  }
]

function LapButton({time}) {

  const [list, setList] = useState(initialLap)

  function handleLap(){

    setList((prevLap)=>[
      ...prevLap,
      {
        min : ("0" + Math.floor((time / 60000) % 60)).slice(-2),
        sec : ("0" + Math.floor((time / 1000) % 60)).slice(-2),
        milli: ("0" + Math.floor(time / 100) % 10).slice(-2)
      }
    ])
  }
  return (
    <>
      <div>
        <button className='btn lap-btn' onClick={()=>handleLap()}>LAP</button>
      </div>
      <div>
        {
          list.map((val)=>(
            <div className='laps'>
              <span>{val.min}:</span>
              <span>{val.sec}:</span>
              <span>{val.milli}</span>
            </div>
          ))
          // <div className='Laps'>
          //   <span>{("0" + Math.floor((snap / 60000) % 60)).slice(-2)}:</span>
          //   <span>{("0" + Math.floor((snap / 1000) % 60)).slice(-2)}:</span>
          //   <span>{("0" + Math.floor(snap / 100) % 10).slice(-2)}</span>
          // </div>
        }
      </div>
    </>
  )
}

export default LapButton