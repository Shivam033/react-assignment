import React from 'react'
import './style.css'

function Timer({time}) {
  return (
    <div>
        <div className='timer'>
          <span>{("0" + Math.floor((time / 60000) % 60)).slice(-2)}:</span>
          <span>{("0" + Math.floor((time / 1000) % 60)).slice(-2)}:</span>
          <span>{("0" + Math.floor(time / 100) % 10).slice(-2)}</span>
        </div>
    </div>
  )
}

export default Timer