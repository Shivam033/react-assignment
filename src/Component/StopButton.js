import React from 'react'
import './style.css'

function StopButton({timeFlag, setTimeFlag}) {

  function handleStop(){
    if(timeFlag){
      setTimeFlag(false);
    }
  }
  return (
    <div>
          <button className=' btn stop-btn' onClick={()=>handleStop()}>STOP</button>
    </div>
  )
}

export default StopButton