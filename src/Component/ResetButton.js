import React from 'react'
import './style.css'

function ResetButton({timeFlag, setTime, time}) {

  function handleReset(){
    if(!timeFlag && time > 0){
      setTime(0);
    }
  }
  return (
    <div>
        <button className='btn reset-btn' onClick={()=>handleReset()}>RESET</button>
    </div>
  )
}

export default ResetButton