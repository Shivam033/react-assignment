import React from 'react'
import './style.css'

function StartButton({timeFlag, setTimeFlag}) {

  function handleStart(){
    if(!timeFlag){
      // console.log(timeFlag);
      setTimeFlag(true);
    }
  }

  return (
    <div>
        <button className='btn start-btn' onClick={()=>handleStart()}>START</button>
    </div>
  )
}

export default StartButton