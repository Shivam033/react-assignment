import './App.css';
import React, { useEffect, useState } from 'react'
import StartButton from './Component/StartButton';
import StopButton from './Component/StopButton';
import ResetButton from './Component/ResetButton';
import LapButton from './Component/LapButton';
import Timer from './Component/Timer';

function App() {
  const [time, setTime] = useState(0);
  const [timeFlag, setTimeFlag] = useState(false);

  useEffect(() =>{
    let interval = null;

    if(timeFlag){
      interval = setInterval(() => {
        setTime(prevTime => prevTime + 10)
      }, 10);
    }else{
      clearInterval(interval);
    }

    return () => clearInterval(interval);

  },[timeFlag])
  return (
    <>
      <div className='App'>
        <img className='stopwatch-icon' src='/stopwatch.svg' alt='stopwatch'/>
        <Timer time={time} />
        <StartButton timeFlag={timeFlag} setTimeFlag={setTimeFlag} />
        <StopButton timeFlag={timeFlag} setTimeFlag={setTimeFlag} />
        <ResetButton timeFlag={timeFlag} setTime={setTime} time={time}/>
        <LapButton time={time}/>
      </div>
    </>
  );
}

export default App;
